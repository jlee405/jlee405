#=========
#Variables
#=========

CFLAGS = -std=c99 -Wall -Wextra -pedantic -O0
LFLAGS = 
CC = gcc

GPROF = -pg
GCOV = -fprofile-arcs -ftest-coverage

#=========
#Rules
#=========


exercise: exercise.c
	$(CC) $(CFLAGS) exercise.c -o exercise


clean:
	rm -rf *.o exercise game *.gcov *.gcda *.gcno gmon.out
